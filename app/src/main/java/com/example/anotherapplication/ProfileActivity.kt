package com.example.anotherapplication

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_profile.*

class ProfileActivity : AppCompatActivity() {
    val requestCode = 20
    val myInfo = UserModel("Lika", "Glonti", "lika.glonti.1@btu.edu.ge", 2001, "Female")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        init()
        firstInfo(myInfo)
    }


    private fun init() {
        changeInfoButton.setOnClickListener {
            getChangedParameters()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == this.requestCode && resultCode == Activity.RESULT_OK) {
            val changedInformation = data?.extras?.getParcelable<UserModel>("UserInformation")
            nameTextView.text = changedInformation!!.name
            lastNameTextView.text = changedInformation.lasName
            emailTextView.text = changedInformation.email
            birthDateTextView.text = changedInformation.birthDate.toString()
            genderTextView.text = changedInformation.gender
        }

    }

    private fun getChangedParameters() {
        val intent = Intent(this, ChangeParametersActivity::class.java)
        startActivityForResult(intent, requestCode)
    }


    private fun firstInfo(UserModel: UserModel) {
        nameTextView.text = UserModel.name
        lastNameTextView.text = UserModel.lasName
        emailTextView.text = UserModel.email
        birthDateTextView.text = UserModel.birthDate.toString()
        genderTextView.text = UserModel.gender

    }


}


