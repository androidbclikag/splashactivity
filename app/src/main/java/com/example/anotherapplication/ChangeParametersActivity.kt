package com.example.anotherapplication

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_change_parameters.*

class ChangeParametersActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_parameters)
        init()
    }


    private fun init() {
        saveChangesButton.setOnClickListener {
            addUserInfo()
        }

    }


    private fun addUserInfo() {

        val userProfile = UserModel(
            nameEditText.text.toString(),
            lastNameEditText.text.toString(),
            emailEditText.text.toString(),
            birthDateEditText.text.toString().toInt(),
            genderEditText.text.toString()
        )
        val intent = intent.putExtra("UserInformation", userProfile)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }


}
